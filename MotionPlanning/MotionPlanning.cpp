#include <iostream>
#include <cstdlib>
#include <climits>
#include <iomanip>
using namespace std;
#define SIZE 10
#define PEROBS 30

typedef struct{
  int x,y;
}*point, point_s;

//Prototypes
void InitializeWorkspace();
void DisplayWorkspace();
void GenerateObstacles();
point Minnext(int x, int y);
void Path();
void Navigation(int x, int y);

int workspace[SIZE][SIZE];

point start,goal;

int main(int argc, const char* argv[]) {

  start = (point)malloc(sizeof(point_s));
  goal = (point)malloc(sizeof(point_s));

  InitializeWorkspace();

  GenerateObstacles();

  DisplayWorkspace();

  //Legend
  cout << "Legend :\nX : Obstacle\nU : Unreachable or navigation function not calculated\n o : Shortest path\n";

  //Ask user for goal
  do{
  cout << "Enter goal latitude :";
  cin >> goal->x;
  cout << "Enter goal longitude :";
  cin >> goal->y;
  if(workspace[goal->x][goal->y] == -1)
    cout << "Your robot is a flying pony, it sets obstacles as objectives, which makes it 20% cooler\n Seriously, get another...\n";
  }while(workspace[goal->x][goal->y] == -1);

  workspace[goal->x][goal->y]=0;

  //Compute navigation function
  Navigation(goal->x, goal->y);

  DisplayWorkspace();

  //Ask user for start
  do{
  cout << "Enter start latitude :";
  cin >> start->x;
  cout << "Enter start longitude :";
  cin >> start->y;
  if(workspace[start->x][start->y] == -1)
    cout << "Your robot is a flying pony, it starts on obstacles, which makes it a bit dumb, get another...\n";
  }while(workspace[start->x][start->y] < 0);

  //Find Path
  Path();

  DisplayWorkspace();
}


void DisplayWorkspace() {
  cout << "    ";
  for(int i=0; i<SIZE; i++)
    cout << setw(2) << i << " ";
  cout << "\n";

  for(int i=0; i<(3*SIZE); i++)
    cout << "-";

  for(int i=0; i<SIZE; i++){
    cout << "\n" << i << " | ";
    for(int j=0; j<SIZE; j++){
      if(workspace[i][j] == -3)
        cout << " o ";
      else if(workspace[i][j] == -2)
        cout << " U ";
      else if(workspace[i][j] == -1)
        cout << " X ";
      else if(workspace[i][j] == 0)
        cout << " G ";
      else
        cout << setfill('0') << setw(2) << workspace[i][j] << " ";
    }
  }
  cout << "\n";
}

void GenerateObstacles() {
  srand(time(NULL));
  for (int i=0 ; i<SIZE ; i++) {
    for (int j=0 ; j<SIZE ; j++) {
      if (rand()%100 < PEROBS) {
        workspace[i][j] = -1;
      }
    }
  }
}

void InitializeWorkspace() {
  for(int i=0; i<SIZE; i++) {
    for(int j=0; j<SIZE; j++) {
      workspace[i][j] = -2;
    }
  }
}

void Navigation(int x, int y)
{
	//casting recursivity on the neighbours
	//and define the value of the neighbours if it has't been done
	//and the case isn't an obstacle
	if 	(x < SIZE-1 && ((workspace[x+1][y] > workspace[x][y] +1) || (workspace[x+1][y] == -2)))
	{
		workspace[x+1][y] = workspace[x][y] + 1;
		Navigation(x+1, y);
	}
	if 	(y < SIZE-1	&& ((workspace[x][y+1] > workspace[x][y] +1) || (workspace[x][y+1] == -2)))
	{
		workspace[x][y+1] = workspace[x][y] + 1;
		Navigation(x, y+1);
	}
	if 	(x > 0 && ((workspace[x-1][y] > workspace[x][y] +1)	|| (workspace[x-1][y] == -2)))
	{
		workspace[x-1][y] = workspace[x][y] + 1;
		Navigation(x-1, y);
	}
	if 	(y > 0 && ((workspace[x][y-1] > workspace[x][y] +1) || (workspace[x][y-1] == -2)))
	{
		workspace[x][y-1] = workspace[x][y] + 1;
		Navigation(x, y-1);
	}
}

void Path()
{
	point p = (point)malloc(sizeof(point_s));
	p->x = start->x; p->y = start->y;

  while (!(p->x == goal->x && p->y == goal->y))
	{
		workspace[p->x][p->y] = -3;
		p = Minnext(p->x, p->y);
	}
}

point Minnext(int x, int y)
{
	int result = INT_MAX;
	point p = (point)malloc(sizeof(point_s));
	p->x = x; p->y = y;

	if ((x < SIZE-1) && (result > workspace[x][y+1]) && (workspace[x+1][y] > -1))	{
		result = workspace[x+1][y];
		p->x = x+1;
	}
	if ((y < SIZE-1) && (result > workspace[x][y+1]) && (workspace[x][y+1] > -1))	{
			result = workspace[x][y+1];
			p->x = x; p->y = y+1;
		}
	if ((x > 0) && (result > workspace[x-1][y]) && (workspace[x-1][y] > -1)) {
			result = workspace[x-1][y];
			p->x = x-1; p->y = y;
		}
	if ((y > 0) && (result > workspace[x][y-1])&& (workspace[x][y-1] > -1)) {
			result = workspace[x][y-1];
			p->x = x; p->y = y-1;
		}

	return p;
}
