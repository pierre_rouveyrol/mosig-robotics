#include <iostream>
#include <cmath>

class Kalman{
    public:
		float mean;
		float std;
		float q;//noise associated to motion
		float r;//noise associated to observation
		float k;
		
		// Eigene Werte
		float a; // Action ([t-1] - [t-2])
		float a_t1; 
		float mean_t2; 	// Mean für [t-2]
		float std_t1;

		Kalman(float init_mean, float init_std, float init_q, float init_r);

		// void prediction(int a);
		void prediction();
		void prediction(float a);
		void estimation(int o);
};
