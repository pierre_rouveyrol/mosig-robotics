#include "Kalman.hpp"

// Uncertaintys: q=Detections & r=Motion
Kalman::Kalman(float init_mean, float init_std, float init_q, float init_r) {
  // Initialize variables
  mean = init_mean;
  mean_t2 = 0;   
  std = std_t1 = init_std;
  q = init_q;
  r = init_r;  
}

void Kalman::prediction() {
  // Action
  a_t1 = a;
  a = mean - mean_t2;
  prediction(a);
}

void Kalman::prediction(float a) {    
  // Preserve for next round
  mean_t2 = mean; 
  std_t1 = std;
  
  // Prediction
  mean += a;
  std += r;
}

void Kalman::estimation(int o) {
  // Q(t)
  r = 0.7*r + 0.5*std::abs(a - a_t1);
  q = 0.7*q + 0.5*std::abs(a - a_t1);
  
  // Kalman Gain
  k = std_t1 / (std_t1 + q);
  
  // Estimation
  mean = mean + k * (o - mean);
  std = (1-k)*std;  
}
