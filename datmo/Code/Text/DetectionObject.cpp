#include <iostream>
/*Libraries needed*/
#include "DataReader.hpp"

int main(int argc, char* argv[]){

  std::string setName(argv[1]);
  DataReader dataObj(setName);
  int key;

  // read the first data to determine the background
  dataObj.readData();//read the next laser data
  dataObj.initBackground();//define the background


  // Displaying the dataset (each sliding window; each frame)
  while(dataObj.readData() == NOERROR) {

    //dataObj.printLaserData();//display the current laser data on the graphical window
    int threshold = 15;
    dataObj.detectMotion(threshold);
    dataObj.printMotion();
    dataObj.formObject();

  }

  return 0;

}
